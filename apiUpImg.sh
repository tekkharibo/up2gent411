#!/bin/sh

base64 -w0 "$1" > "$1".64.jpg
apiXml=$(curl -v -F "upload=<$1.64.jpg" -F key="$apiKey" -F format=xml "$apiUrl")
read imgView < <(xmlstarlet sel -t -m '//image_viewer' -v . -n <<< "$apiXml")
read imgThumb < <(xmlstarlet sel -t -m '//image_thumb_url' -v . -n <<< "$apiXml")
imgLink="[url=$imgView][img]$imgThumb[/img][/url]"
echo "$imgLink"

