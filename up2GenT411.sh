#!/bin/bash

# --------------------------------------------------------------------------
# --- CONFIGURATION --------------------------------------------------------
# --------------------------------------------------------------------------

# Liste des Extention gérées
VIDEO_EXTENTION="avi mkv wmv mp4 mpg mpeg mov m4v"

# Creation de'Image de Vignette (1/0)
VIGNETTE_CREATE=1

# Upload des vignettes (1/0)
VIGNETTE_UPLOAD=0

# Informations sur l'API d'hébergement d'image
APIKEY=
APIURL=

# Config Build-Torrent
TORRENT_BIN="buildtorrent"
TORRENT_URL="http://tracker.t411.me:56969/announce"
TORRENT_OPT="-q -p 1 -a $TORRENT_URL"

# Config Media Info
NFO_BIN="mediainfo"
NFO_PREZ="--Inform=file:///$HOME/minfo.txt"

# Config Movie Thumbnail
VIGNETTE_BIN="mtn"
VIGNETTE_FONT="/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf"
VIGNETTE_OPT="-f $VIGNETTE_FONT -c 3 -j 70 -r 8 -w 1024"

# Répertoire de destination
DEST_DIR="$HOME/upload"


# --------------------------------------------------------------------------
# --- FONCTIONS ------------------------------------------------------------
# --------------------------------------------------------------------------

# Calcul de la taille des pièces du torrent
function pieceSize () {
    filesize=$(du -m --max-depth=0 "$1" | awk '{print $1}')
    if [ $filesize -lt 150 ]; then
		piece=16
    elif [ $filesize -lt 350 ]; then
		piece=17
    elif [ $filesize -lt 500 ]; then
		piece=18
    elif [ $filesize -lt 1000 ]; then
		piece=19
    elif [ $filesize -lt 10000 ]; then
		piece=20
    elif [ $filesize -lt 20000 ]; then
		piece=21
    else
		piece=22
    fi
}

# Upload des screenshots et création du lien bbcode
function screenUp () {
	base64 -w0 "$1" > "$1".64.jpg
	apiXml=$(curl -v -F "upload=<$1.64.jpg" -F key="$APIKEY" -F format=xml "$APIURL")
	rm "$1".64.jpg
	read imgView < <(xmlstarlet sel -t -m '//image_viewer' -v . -n <<< "$apiXml")
	read imgThumb < <(xmlstarlet sel -t -m '//image_thumb_url' -v . -n <<< "$apiXml")
	imgLink="[url=$imgView][img]$imgThumb[/img][/url]"
	echo "$imgLink" >> "$prez"
}

# Dans le cas d'un fichier unique
function genFile() {
    filename="$1"
    base=${filename##*/}
    dir="${filename%$base}"
    ext=${filename##*.}
    prez="$DEST_DIR/$base.txt"
    if [ -z "$dir" ]; then
		dir=".";
    fi
    echo $filename
	echo "[center][font=Georgia]" > "$prez"
    echo "IMG" >> "$prez"
    echo "[img]http://prez411.me/images/2013/09/19/prez.gif[/img]" >> "$prez"
    echo "[size=2]Clique pour agrandir[/size]" >> "$prez"
    if [[ "$VIDEO_EXTENTION" == *"$ext"* && $VIGNETTE_CREATE -eq 1 ]]; then
		$VIGNETTE_BIN $VIGNETTE_OPT "$filename" -O "$DEST_DIR" 2>/dev/null
		if [ $VIGNETTE_UPLOAD = 1 ]; then
			screenUp "$DEST_DIR/${filename%.*}_s.jpg"
		fi
    fi
    echo "" >> "$prez"
    echo "[img]http://prez411.me/images/2013/09/19/descriptionmEASs.gif[/img]" >> "$prez"
    echo "" >> "$prez"
    echo "DESCRIPTION" >> "$prez"
    echo "" >> "$prez"
    echo "[img]http://prez411.me/images/2013/09/19/codecaY5G8.gif[/img]" >> "$prez"
    echo "" >> "$prez"
    $NFO_BIN $NFO_PREZ "$filename" >> "$prez"
    echo "[/font][/center]" >> "$prez"
    $NFO_BIN "$filename" > "$DEST_DIR/$base.nfo"
    pieceSize "$1"
    $TORRENT_BIN $TORRENT_OPT -L $piece "$filename" "$DEST_DIR/$base.torrent"
}

# Dans le cas d'un répertoire
function genDir() {
    dir=$1
    base=${dir##*/}
    prez="$DEST_DIR/$base/$base.txt"
    pieceSize "$1"
    mkdir "$DEST_DIR/$base"
    $TORRENT_BIN $TORRENT_OPT -L $piece "$dir" "$DEST_DIR/$base/$base.torrent" 
	echo > "$DEST_DIR/$base/$base.nfo"
    echo "[center][font=Georgia]" > "$prez"
    echo "" >> "$prez"
    echo "IMG" >> "$prez"
    echo "[img]http://prez411.me/images/2013/09/19/prez.gif[/img]" >> "$prez"
        for type in $VIDEO_EXTENTION; do
		ls -1 $dir/*.$type 2>/dev/null | while read -r file; do
			echo $file
			fbase=${file##*/}
			fdir=${file%$base}
			fext=${file##*.}
			$NFO_BIN $NFO_PREZ "$file" >> "$prez"
			if [[ "$VIDEO_EXTENTION" == *"$fext"* && $VIGNETTE_CREATE -eq 1 ]]; then
				$VIGNETTE_BIN $VIGNETTE_OPT "$file" -O "$DEST_DIR/$base" 2>/dev/null
				echo "[size=2]Clique pour agrandir[/size]" >> "$prez"
				if [ $VIGNETTE_UPLOAD = 1 ]; then
					screenUp "$DEST_DIR/${file%.*}_s.jpg"
				fi
			fi
			echo "------------------------------" >> "$prez"
			echo "" >> "$prez"
		done
    done
    echo "" >> "$prez"
    echo "[img]http://prez411.me/images/2013/09/19/descriptionmEASs.gif[/img]" >> "$prez"
    echo "" >> "$prez"
    echo "DESCRIPTION" >> "$prez"
    echo "" >> "$prez"
    echo "[img]http://prez411.me/images/2013/09/19/codecaY5G8.gif[/img]" >> "$prez"
    echo "" >> "$prez"
    for type in $VIDEO_EXTENTION; do
		ls -1 $dir/*.$type 2>/dev/null | while read -r file; do
			echo $file
			fbase=${file##*/}
			fdir=${file%$base}
			fext=${file##*.}
			$NFO_BIN "$file" >> "$DEST_DIR/$base/$base.nfo"
			echo "------------------------------" >> "$DEST_DIR/$base/$base.nfo"
		done
    done
    echo "[/font][/center]" >> "$prez"
}

function usage() {
    echo "gen2upload v1.0"
    echo "Creation automatique des fichier NFO, Torrent et Vignettes (videos) pour la préparation de vos upload"
    echo "(voir la configuration en debut du script)"
    echo -e "Usage: gen2upload.sh [media|dossier]"
    echo -e "Exemples Argument Fichier:"
    echo " gen2upload.sh MaVideo.avi"
    echo " gen2upload.sh MaMusique.mp3"
    echo " gen2upload.sh /home/user/MaVideo.avi"
    echo " gen2upload.sh 'Ma Video avec espace.avi'"
    echo " gen2upload.sh '/home/user/Ma Video avec espace.avi'"
    echo -e "Exemples Argument Fichier:"
    echo " gen2upload.sh MonDossier"
    echo " gen2upload.sh /home/user/MonDossier'"
    echo ""
    echo -e "Notes: Cette pre-version ne fonctionne pas avec un argument dossier comportant des espaces..."
    echo "Prérequis: MediaInfo, buildtorrent, mtn (movie thumbnailer)"
    echo ""
}

# --------------------------------------------------------------------------
# --- EXECUTION ------------------------------------------------------------
# --------------------------------------------------------------------------

if [ $# -eq 0 ] ; then
    usage;
else
    if [[ -f $1 ]]; then
		genFile "$1";
    elif [[ -d $1 ]]; then
		genDir "$1";
    else
		usage;
    fi
fi
